import { SharedModule } from '../shared/shared.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsListComponent } from './app-layout-container/app-layout-container.container';
import { ConsoleWindowComponent } from './console-window/console-window.component';
import { ButtonsLayoutComponent } from './buttons-layout/buttons-layout.component';
import { ButtonGeneratorComponent } from './button-generator/button-generator.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
  ],
  declarations: [
    ComponentsListComponent,
    ConsoleWindowComponent,
    ButtonsLayoutComponent,
    ButtonGeneratorComponent,
  ],
  exports: [ComponentsListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ComponentsModule { }
