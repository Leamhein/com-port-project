import { Component, OnInit } from '@angular/core';
import { ButtonCreatorService } from '../../api/services/button-creator.service';

@Component({
  selector: 'app-button-generator',
  templateUrl: './button-generator.component.html',
  styleUrls: ['./button-generator.component.scss']
})
export class ButtonGeneratorComponent implements OnInit {

  public btnNamePlaceholder = 'Type button name here';
  public commandPlaceholder = 'Type command here';
  public btnName: string | null = null;
  public userCommand: string | null = null;
  public alert: string | null = null;
  public isSucceed: boolean;

  constructor(
    private buttonCreator: ButtonCreatorService,
  ) { }

  ngOnInit() {
  }

  public onClick() {
    if (this.btnName && this.userCommand) {
      this.buttonCreator.createButton(this.btnName, this.userCommand);
      this.alert = 'Succeed!';
      this.isSucceed = true;
      setTimeout(() => this.alert = null, 2000);
    }
    if (!this.btnName) {
      this.isSucceed = false;
      this.alert = `Can't create button without name!`;
    }
    if (!this.userCommand) {
      this.isSucceed = false;
      this.alert = `Can't create button without command!`;
    }
  }
}
