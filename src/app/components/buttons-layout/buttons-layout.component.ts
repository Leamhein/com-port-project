import { Component, OnInit } from '@angular/core';
import { ButtonCreatorService } from '../../api/services/button-creator.service';
import { ActionButton } from '../../models/button.model';
import { CashService } from '../../api/services/cash.service';
import { ElectronService } from '../../api/services/electron.service';

const KEY = 'buttons';

@Component({
  selector: 'app-buttons-layout',
  templateUrl: './buttons-layout.component.html',
  styleUrls: ['./buttons-layout.component.scss']
})
export class ButtonsLayoutComponent implements OnInit {
  public buttons: ActionButton[] = [];

  constructor(
    private buttonCreator: ButtonCreatorService,
    private cashService: CashService,
    public electronService: ElectronService,
  ) { }

  ngOnInit() {
    const cashedData = this.cashService.getData(KEY);
    if (cashedData) {
      this.buttons = cashedData.concat(this.buttons);
    }
    this.buttonCreator.getButton().subscribe(
      button => {
        this.buttons.push(button);
        this.cashService.cashData(KEY, this.buttons);
      });
    // this.electronService.serialPort.list().then(ports => {
    //   console.log('[LOG] List of ports: ', ports);
    // });
  }

  public onClick(cmd: string) {
    console.log(cmd);
  }

  public deleteCashedButtons(): void {
    this.cashService.deleteCash(KEY);
  }

  public deleteBtn(button: ActionButton) {
    this.buttons = this.buttons.filter(btn => !(button === btn));
  }
}
