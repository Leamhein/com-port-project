import { TestBed, inject } from '@angular/core/testing';

import { ButtonCreatorService } from './button-creator.service';

describe('ButtonCreatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ButtonCreatorService]
    });
  });

  it('should be created', inject([ButtonCreatorService], (service: ButtonCreatorService) => {
    expect(service).toBeTruthy();
  }));
});
