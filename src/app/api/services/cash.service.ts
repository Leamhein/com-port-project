import { Injectable } from '@angular/core';
import { ActionButton } from '../../models/button.model';

@Injectable({
  providedIn: 'root'
})
export class CashService {
  public cashData(key: string, data: ActionButton[]): void {
    localStorage.setItem(key, JSON.stringify(data));
  }

  public getData(key: string): ActionButton[] {
    return JSON.parse(localStorage.getItem(key));
  }

  public deleteCash(key: string): void {
    localStorage.removeItem(key);
  }
}
