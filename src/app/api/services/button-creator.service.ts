import { Injectable } from '@angular/core';
import { ActionButton } from '../../models/button.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ButtonCreatorService {
  public getButton$: Subject<ActionButton> = new Subject();

  constructor() {}

  public getButton(): Subject<ActionButton> {
    return this.getButton$;
  }

  public createButton(name: string, command: string): void {
    const button = new ActionButton({
      name,
      command,
    });

    this.getButton$.next(button);
  }
}
