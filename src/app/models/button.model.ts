export class ActionButton {
  public name: string;
  public command: string;

  constructor(params: ActionButton) {
    Object.assign(this, params);
  }
}
